<?php

namespace App\Http\Controllers\Api;

use App\City;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DateController extends Controller
{
    public function search(Request $request)
    {
        $startDate = request('startDate');
        $endDate = request('endDate');

        // manipulate query di sini
        $records = City::whereDate('created_at', '>=', $startDate)
            ->whereDate('created_at', '<=', $endDate)
            ->get();

        $myData = [
            'startDate' => $startDate,
            'endDate' => $endDate,
            'records' => $records
        ];

        return $myData;
    }
}
