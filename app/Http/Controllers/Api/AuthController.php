<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;       // import model User (yg bercakap dengan table users di DB)
use Auth;

class AuthController extends Controller
{
    //app/Http/Controllers/Api/AuthController

    // untuk reset password for mobile app
    public function resetPassword(Request $request)
    {
        // kita nak terima email yang di hantar oleh app
        $email = request('email');
        // kita nak check email tersebut ada ke tak dalam database
        $emailExist = User::where('email', $email)->count();
        // kalau ada, kita return mesej, suruh orang tu klink reset link di email dia
        // else, kita kata email tersebut tidak wujud

        if ($emailExist > 0) {
            // kita send email reset link pada mailbox user
            return response()->json('Sila klik pada link reset password di email anda', 200);
        } else {
            return response()->json('Email tersebut tidak wujud', 200);
        }
    }

    // untuk process login bagi mobile app
    public function login(Request $request)
    {
        $username = request('username');
        $password = request('password');

        if (Auth::attempt(['email' => $username, 'password' => $password])) {
            // login berjaya
            $user = Auth::user();
            $data = [
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                'role' => $user->role,
                'avatar' => env('APP_URL').$user->avatar,
                'status' => 'pass',
                'token' => encrypt($user->id)
            ];
        } else {
            // login gagal
            $data = [
                'status' => 'fail',
                'message' => 'Nama pengguna atau kata laluan tidak sah.'
            ];
        }

        return response()->json($data, 200);

        // just to check, our function boleh capture value yang di hantar atau tidak
//        return response()->json("Username: $username & Password: $password", 200);
    }

}
