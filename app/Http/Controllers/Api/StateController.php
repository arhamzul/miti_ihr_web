<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\State;      // model untuk cakap dengan table 'states'
use App\User;

class StateController extends Controller
{
    // get states based on country
    public function stateByCountry(Request $request)
    {
        // Get token & countryId from mobile app
        $token = request('token');
        $countryId = request('countryId');

        $decryptedToken = decrypt($token);

        // get record user yang terkini (id paling besar)
        $user = User::orderBy('id', 'desc')->first();
        $latestId = $user->id;

        if ($decryptedToken > 0 && $decryptedToken <= $latestId) {
            // token valid, so get the data
            $states = State::where('country_id', $countryId)->get();

            // construct data kita supaya mudah nak digunakan oleh mobile app
            $myData = [
                'token' => $token,
                'decryptedToken' => $decryptedToken,
                'status' => 'ok',
                'states' => $states
            ];

            return $myData;
        } else {
            $myData = [
                'status' => 'fail',
                'message' => 'Invalid token.'
            ];

            return $myData;
        }

    }
}
