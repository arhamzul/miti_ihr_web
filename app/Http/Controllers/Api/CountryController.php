<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Country;

class CountryController extends Controller
{
    // senarai semua negara
    public function allCountries(Request $request)  // kena ada Request, untuk capture value dari Postmen atau API
    {
        // Get token from mobile app
        $token = request('token');
        $decryptedToken = decrypt($token);

        // get record user yang terkini (id paling besar)
        $user = User::orderBy('id', 'desc')->first();
        $latestId = $user->id;

        if ($decryptedToken > 0 && $decryptedToken <= $latestId) {
            // token valid, so get the data
            // Get all countries from database :: select * from countries;
            $countries = Country::all();

            // construct data kita supaya mudah nak digunakan oleh mobile app
            $myData = [
                'token' => $token,
                'decryptedToken' => $decryptedToken,
                'status' => 'ok',
                'countries' => $countries
            ];

            return $myData;
        } else {
            $myData = [
                'status' => 'fail',
                'message' => 'Invalid token.'
            ];

            return $myData;
        }
    }
}
