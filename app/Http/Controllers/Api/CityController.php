<?php

namespace App\Http\Controllers\Api;

use App\City;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class CityController extends Controller
{
    public function cityByState(Request $request)
    {
        // Get token from mobile app
        $token = request('token');
        $stateId = request('stateId');
        $decryptedToken = decrypt($token);
        // get record user yang terkini (id paling besar)
        $user = User::orderBy('id', 'desc')->first();
        $latestId = $user->id;
        if ($decryptedToken > 0 && $decryptedToken <= $latestId) {
            $cities = City::where('state_id', $stateId)->get();
            // construct data kita supaya mudah nak digunakan oleh mobile app
            $myData = [
                'token' => $token,
                'decryptedToken' => $decryptedToken,
                'status' => 'ok',
                'cities' => $cities
            ];

            return $myData;
        } else {
            $myData = [
                'status' => 'fail',
                'message' => 'Invalid token.'
            ];
            return $myData;
        }
    }
}
