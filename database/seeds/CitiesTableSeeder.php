<?php

use Illuminate\Database\Seeder;
use App\State;
use App\City;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $selangor = State::where('name', 'Selangor')->first();

        $city = new City();
        $city->name = "Shah Alam";
        $city->state_id = $selangor->id;
        $city->save();

        $city = new City();
        $city->name = "Subang";
        $city->state_id = $selangor->id;
        $city->save();

        $city = new City();
        $city->name = "Klang";
        $city->state_id = $selangor->id;
        $city->save();

        $city = new City();
        $city->name = "Rawang";
        $city->state_id = $selangor->id;
        $city->save();

        // Kedah
        $kedah = State::where('name', 'Kedah')->first();

        $city = new City();
        $city->name = "Alor Setar";
        $city->state_id = $kedah->id;
        $city->save();

        $city = new City();
        $city->name = "Sungai Petani";
        $city->state_id = $kedah->id;
        $city->save();

        $city = new City();
        $city->name = "Jitra";
        $city->state_id = $kedah->id;
        $city->save();
//
//
//        // Melaka
        $melaka = State::where('name', 'Melaka')->first();

        $city = new City();
        $city->name = "Bandar Melaka";
        $city->state_id = $melaka->id;
        $city->save();

        $city = new City();
        $city->name = "Ayer Keroh";
        $city->state_id = $melaka->id;
        $city->save();

        $city = new City();
        $city->name = "Bandar Hilir";
        $city->state_id = $melaka->id;
        $city->save();



    }
}
