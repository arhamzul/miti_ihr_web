<?php

use Illuminate\Database\Seeder;
use App\Country;        // model Country yg cakap dengan table countries
use App\State;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // for Malaysia
        $malaysia = Country::where('name', 'Malaysia')->first();
        $malaysiaId = $malaysia->id;

        $state = new State();
        $state->name = 'Selangor';
        $state->country_id = $malaysiaId;
        $state->save();

        $state = new State();
        $state->name = 'Kedah';
        $state->country_id = $malaysiaId;
        $state->save();

        $state = new State();
        $state->name = 'Melaka';
        $state->country_id = $malaysiaId;
        $state->save();


        // for Indonesia
        $indonesia = Country::where('name', 'Indonesia')->first();
        $indoId = $indonesia->id;

        $state = new State();
        $state->name = 'Medan';
        $state->country_id = $indoId;
        $state->save();

        $state = new State();
        $state->name = 'Jawa Barat';
        $state->country_id = $indoId;
        $state->save();

        $state = new State();
        $state->name = 'Bali';
        $state->country_id = $indoId;
        $state->save();

        // for Thailand
        $thailand = Country::where('name', 'Thailand')->first();
        $thailandId = $thailand->id;

        $state = new State();
        $state->name = 'Thai State 1';
        $state->country_id = $thailandId;
        $state->save();

        $state = new State();
        $state->name = 'Thai State 2';
        $state->country_id = $thailandId;
        $state->save();

        $state = new State();
        $state->name = 'Thai State 3';
        $state->country_id = $thailandId;
        $state->save();


    }
}
