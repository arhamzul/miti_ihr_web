<?php

use Illuminate\Database\Seeder;
use App\Country;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $country = new Country();
        $country->name = 'Malaysia';
        $country->save();

        $country = new Country();
        $country->name = 'Indonesia';
        $country->save();

        $country = new Country();
        $country->name = 'Singapore';
        $country->save();

        $country = new Country();
        $country->name = 'Thailand';
        $country->save();

        $country = new Country();
        $country->name = 'Vietnam';
        $country->save();
    }
}
