<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// disini kita set route untuk API kita

// 127.0.0.1:8000/api/reset-password
Route::post('reset-password', 'Api\AuthController@resetPassword');
// 127.0.0.1:8000/api/login
Route::post('login', 'Api\AuthController@login');
Route::post('country/all', 'Api\CountryController@allCountries');
Route::post('state/by-country', 'Api\StateController@stateByCountry');
Route::post('city/by-state', 'Api\CityController@cityByState');
Route::post('date/search', 'Api\DateController@search');
